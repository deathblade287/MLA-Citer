# MLA Bibliography
This programme was built to help IGCSE students cite their sources in their GP Individual Report.

**Process Of Citation**
1. Accepts a list of sources and Scrapes them using BS4 and collects author, publishing date, etc. 
2. Saves the information in a csv file
3. Prints out a string of MLA formatted citation that you can copy-paste into your report
