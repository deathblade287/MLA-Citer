import json
from csv import writer

import pandas as pd
import requests
from bs4 import BeautifulSoup


def scrape(links):
    for url in links:
        soup = BeautifulSoup(requests.get(url).content, "html.parser")
        try:
            data = json.loads(
                soup.select_one('[type="application/ld+json"]').contents[0]
            )
        except AttributeError:
            authorname = 'No Author'

        try:
            authorname = data["author"]["name"]
        except KeyError:
            authorname = data["@graph"][2]["author"]["name"]
        finally:
            x = authorname.split()
            lastname = x[1]
            firstname = x[0]
        datePublished = data["datePublished"]
        title = data['headline']

        record = [lastname, firstname, title, 'Forbes', datePublished, url]
    return record


def writeToCSV(record, fileName=r'sources_info.csv'):
    with open(r'sources_info.csv', 'a', newline='') as f:
        writer_object = writer(f)
        writer_object.writerow(record)
        f.close()


def cite_formatting():
    df = pd.read_csv("sources_info.csv")
    sources = []
    for i in range(len(df.index)):
        row = dict(df.iloc[i])
        last_name, first_name, title, website_name, date_of_publication, url = row['lastName'], row[
            'firstName'], row['title'], row['websiteName'], row['dataOfPublication'], row['url']

        final_cite = f'{last_name}, {first_name}. "{title}." {website_name}, {date_of_publication}, {url}'
        sources.append(final_cite)
    return sources


if __name__ == "__main__":
    links = ["https://www.forbes.com/sites/zackfriedman/2021/11/13/the-student-loan-crisis-is-worse-than-you-think/?sh=488e6ee94fd2"]
    record = scrape(links)
    print(record)

    writeToCSV(record)
    print('Record Written To CSV')

    citations = cite_formatting()
    for citation in citations:
        print(citations)
